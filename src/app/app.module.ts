import { EventPage } from './../pages/event/event';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {LoginPage} from "../pages/login/login";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
export const URL='http://localhost:18080/SkiWorld-web/';
//export const URL='http://10.0.3.2:18080/SkiWorld-web/';

import {AllOffersPage} from "../pages/all-offers/all-offers";
export const URL='http://localhost:18080/SkiWorld-web/';
export const BASE_URL = URL+'v0/';
export const USER_IMAGE_DIR =URL+'resources/users/';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    EventPage,
    AllOffersPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    FormsModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    EventPage,
    AllOffersPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
