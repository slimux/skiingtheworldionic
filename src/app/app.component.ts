import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import {LoginPage} from "../pages/login/login";
import {User} from "../models/user/user";
import { EventPage } from '../pages/event/event';
import {AllOffersPage} from "../pages/all-offers/all-offers";
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav)nav:Nav;
  pages:Array<{title:string,component:any,role?:any}>;
  rootPage:any = LoginPage;
  private currentUser:User;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Events', component: EventPage },
      { title: 'Offers', component: AllOffersPage}
    ];
    console.log('not ready');
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      console.log('ready');
      splashScreen.hide();

      try{
        this.currentUser = JSON.parse(localStorage.getItem('current'));
      }catch(e){
          this.currentUser = null;
      }

      if(this.currentUser == null)
        this.rootPage = LoginPage;
      else
        this.rootPage = HomePage;

    });
  }

  public setCurrentUser(user:User){
      this.currentUser = user;
      console.log(this.currentUser);
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  get CurrentUser(){
    return this.currentUser;
  }

  logout(){
    localStorage.setItem('current','');
  }
}

