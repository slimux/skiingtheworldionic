import {Component, Injector} from '@angular/core';
import { NavController } from 'ionic-angular';
import {MyApp} from "../../app/app.component";
import {USER_IMAGE_DIR} from "../../app/app.module";
import {LoginPage} from "../login/login";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private parent:MyApp;
  private avatar :string;
  constructor(public navCtrl: NavController,private inj:Injector) {
    this.parent = this.inj.get(MyApp);
    this.avatar = USER_IMAGE_DIR+this.parent.CurrentUser.imageName;

  }

  logout(){
    this.navCtrl.push(LoginPage);
    this.parent.logout();
  }

  reload(){
    window.location.reload();
  }

}
