import {Component, Injector} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {LoginResponse} from "../../models/LoginResponse";
import {UserService} from "../../services/user.service";
import {MyApp} from "../../app/app.component";
import {HomePage} from "../home/home";


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers:[UserService]
})
export class LoginPage {
  private model:any={};
  private loginResponse:LoginResponse;
  private parent:MyApp;

  constructor(public navCtrl: NavController, public navParams: NavParams,private loadingController:LoadingController,
              private us:UserService,private inj:Injector,private toastCtrl:ToastController) {
    this.parent = this.inj.get(MyApp);
  }

  ionViewDidLoad() {

  }

  public doLogin(loginModel:any){


    let loader = this.loadingController.create({
      content: "Please wait..."

    });
    loader.present();
    this.us.login(loginModel).subscribe(ok=>{
      loader.dismissAll();
      this.loginResponse = ok.json();
      if (this.loginResponse.code == 0){
        this.parent.setCurrentUser(this.loginResponse.token.user);
        localStorage.setItem('current',JSON.stringify(this.loginResponse.token.user));
            this.navCtrl.push(HomePage);
      }
      else{
          this.toastCtrl.create({
            message:this.loginResponse.message,
            duration:3000
          }).present();
      }
    },
      error=>{
      loader.dismissAll();
      console.log(error);
      });


  }

}
