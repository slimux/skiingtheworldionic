import { Component, Injector } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { eventService } from '../../services/event.service';
import { event } from '../../models/event/event';

/**
 * Generated class for the EventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-event',
  templateUrl: 'event.html',
  providers: [eventService]
})
export class EventPage {
  events:event[]=[];

  constructor(public navCtrl: NavController, public navParams: NavParams,private es:eventService,private inj:Injector) {
    this.getEvent();
  }

  getEvent(){
    this.es.getAllEvents().subscribe(data =>{
      this.events = data.json();
      console.log(this.events);
    });
  }
  getItems(ev) {
    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.events = this.events.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }else{
      this.getEvent();
    }
  }
  

}
