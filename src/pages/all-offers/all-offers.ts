import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import {JobOffersService} from "../../services/jobOffers.service";
import { JobOffer } from '../../models/offers/offer';

/**
 * Generated class for the AllOffersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-all-offers',
  templateUrl: 'all-offers.html',
  providers: [JobOffersService]

})
export class AllOffersPage {
  offers:JobOffer[]=[];

  constructor(public navCtrl: NavController, public navParams: NavParams,private jos:JobOffersService) {
    this.getOffer();
  }



  getOffer(){
    this.jos.getAllOffers().subscribe(data =>{
      this.offers = data.json();
      console.log(this.offers);
    });
  }

  getItems(jo) {
    var val = jo.target.value;

    if (val && val.trim() != '') {
      this.offers = this.offers.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }else{
      this.getOffer();
    }
  }

}
