import { AccessToken } from "./accesstoken";

export class LoginResponse{
    constructor(public code:number,public token:AccessToken,public message:string){

    }
}
