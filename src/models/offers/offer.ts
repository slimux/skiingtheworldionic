import { User} from "../user/user";
export class JobOffer {
  constructor(public id: number,
              public creationDate:Date,
              public description: string,
              public  endDate: Date,
              public isArchived: boolean,
              public jobOfferCategory: string,
              public name: string,
              public numberOfPlaces :number,
              public salary:number,
              public startDate:Date,
              public agent:User
  ) {

  }
}
